package com.techu.backend.controller;

import com.techu.backend.model.ProductoModel;
import com.techu.backend.servicio.ProductoMongoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("${url.base}")
public class ProductoController {

    @Autowired
    private ProductoMongoService productoService;

    @GetMapping("")
    public String index(){
        return "API REST TECHU v2.0";
    }

    //GET a todos los productos
    @GetMapping("/products")
    public List<ProductoModel> getProducts(){
        return productoService.findAll();
    }

    //POST crear producto
    @PostMapping("/products")
    public ResponseEntity postProducts(@RequestBody ProductoModel newProduct){
        productoService.save(newProduct);
        return new ResponseEntity<>("Producto creado correctamente",HttpStatus.CREATED);
    }

    //GET a un único producto
    @GetMapping("/products/{id}")
    public Optional<ProductoModel> getProductsById(@PathVariable String id){
        return productoService.findById(id);
    }

    //PUT
    @PutMapping("/products")
    public void putProducto (@RequestBody ProductoModel newProduct){
        productoService.save(newProduct);
    }

    //DELETE borrar producto
    @DeleteMapping ("/products/")
    public boolean deleteProduct(@RequestBody ProductoModel newProduct){
        return productoService.delete(newProduct);
    }


}
