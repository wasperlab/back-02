package com.techu.backend.servicio;

import com.techu.backend.model.ProductoModel;
import com.techu.backend.repository.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class ProductoMongoService {

    @Autowired
    ProductoRepository productoRepository;

    //READ
    public List<ProductoModel> findAll(){
        return productoRepository.findAll();
    }

    //CREATE
    public ProductoModel save (ProductoModel newProducto){

        return productoRepository.save(newProducto);
    }

    //READ ID
    public Optional<ProductoModel> findById(String id)
    {
     return productoRepository.findById(id);
    }

    //DELETE
    public boolean delete(ProductoModel producto) {
        try {
          productoRepository.delete(producto);
           return true;
        } catch (Exception ex){
            return false;
        }
    }




}

